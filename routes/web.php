<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\MyController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */
/* Route::view('/', 'home')->middleware('language'); */

/* Route::get('auth/login', function () {
    $title = 'Login';
    return view('auth/login', compact('title'));
});

Route::get('auth/logout', function () {
    echo "Logout usuario";
}); */

Route::get('generate-pdf', [PDFController::class, 'generatePDF']);

// Excel
Route::get('importExportView', [MyController::class, 'importExportView']);
Route::get('export', [MyController::class, 'export'])->name('export');
Route::post('import', [MyController::class, 'import'])->name('import');

Auth::routes(['verify' => 'true']);

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
});

Route::group(['middleware' => 'verified'], function () {

    Route::get('catalog', [CatalogController::class, 'getIndex']);

    Route::get('catalog/show/{id}', [CatalogController::class, 'getShow']);
    
    // Create
    Route::get('catalog/create', [CatalogController::class, 'getCreate'])->middleware(['password.confirm']);
    Route::post('catalog/create', [CatalogController::class, 'postCreate']);
    
    // Edit
    Route::get('catalog/edit/{id}', [CatalogController::class, 'getEdit'])->middleware(['password.confirm']);
    Route::put('catalog/edit/{id}', [CatalogController::class, 'putEdit']);
    
    Route::delete('catalog/delete/{id}', [CatalogController::class, 'putDelete'])->middleware(['password.confirm']);
    
});

Route::middleware(['language', 'verified'])->group(function () {

    Route::get('catalog', [CatalogController::class, 'getIndex']);

    Route::get('catalog/show/{id}', [CatalogController::class, 'getShow']);
    
    // Create
    Route::get('catalog/create', [CatalogController::class, 'getCreate'])->middleware(['password.confirm']);
    Route::post('catalog/create', [CatalogController::class, 'postCreate']);
    
    // Edit
    Route::get('catalog/edit/{id}', [CatalogController::class, 'getEdit'])->middleware(['password.confirm']);
    Route::put('catalog/edit/{id}', [CatalogController::class, 'putEdit']);
    
    Route::delete('catalog/delete/{id}', [CatalogController::class, 'putDelete'])->middleware(['password.confirm']);
    
});

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('felicitar', [MailController::class, 'felicita']);