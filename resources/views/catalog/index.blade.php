@extends('layouts.master')

@section('content')

<div class="row">
    @if(Session::has('correcto'))
    <div class="alert alert-success"> {{ Session::get('correcto') }}</div>
    @endif

    @if(Session::has('editOK'))
    <div class="alert alert-success"> {{ Session::get('editOK') }}</div>
    @endif

    @if(Session::has('deleteOK'))
    <div class="alert alert-success"> {{ Session::get('deleteOK') }}</div>
    @endif

    @foreach($arrayClientes as $key => $cliente)

    <div class="col-xs-6 col-sm-4 col-md-3 text-center">
        <a href="{{url('/catalog/show/' . $cliente->id)}}">
            <img src="{{$cliente->imagen}}" style="width:250px">
            <h4 style="min-height: 45px; margin:5px 0 10px 0;">
                {{$cliente->nombre}}
            </h4>
        </a>
    </div>

    @endforeach

    {{ $arrayClientes->links() }}
</div>


@stop