@extends('layouts.master')

@section('content')

@if ($errors->any())
<div class="row justify-content-center">
   <div class="col-sm-7">
      <div class="alert alert-danger">
         <ul>

            @foreach($errors->all() as $error)

            <li>{{$error}}</li>

            @endforeach

         </ul>

      </div>

   </div>

</div>
@endif

<div class="row" style="margin-top:40px">
   <div class="offset-md-3 col-md-6">
      <div class="card">
         <div class="card-header text-center">
            Modificar cliente
         </div>
         <div class="card-body" style="padding:30px">

            <form action="" method="POST" enctype='multipart/form-data'>
               @method('PUT')

               @csrf

               <div class="form-group">
                  <label for="nombre">Nombre</label>
                  <input type="text" name="nombre" id="nombre" class="form-control" value="{{$cliente->nombre}}">
               </div>

               <div class="form-group">
                  <label for="imagen">Imagen</label>
                  <input type="file" name="imagen" id="imagen" class="form-control">
               </div>

               <div class="form-group">
                  <label for="fecha_nacimiento">Fecha de nacimiento</label>
                  <input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control" placeholder="AAAA-MM-DD" value="{{$cliente->fecha_nacimiento}}">

               </div>

               <div class="form-group">
                  <label for="correo">Email</label>
                  <input type="text" name="correo" id="correo" class="form-control" value="{{$cliente->correo}}">
               </div>

               <div class="form-group text-center">
                  <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                     Modificar cliente
                  </button>
               </div>

            </form>

         </div>
      </div>
   </div>
</div>


@stop