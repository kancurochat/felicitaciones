@extends('layouts.master')
@include('catalog.destroy')
@section('content')
<div class="row">

    <div class="col-sm-4">

        <img class="p-2" src="{{$cliente['imagen']}}" style="width: 350px;">

    </div>

    <div class="col-sm-8">
        <h2>{{$cliente->nombre}}</h2>

        <h3>{{$cliente->fecha_nacimiento}}</h3>

        <h3>{{$cliente->correo}}</h3>

        <a href="/catalog/edit/{{$cliente->id}}" class="btn btn-success">Editar</a>
        <a href="" data-target="#modal-delete-{{$cliente->id}}" data-toggle="modal" class="btn btn-danger ml-2">Eliminar</a>
        <a class="btn btn-primary">Volver</a>
    </div>

</div>

@stop