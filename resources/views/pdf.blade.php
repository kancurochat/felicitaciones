<!DOCTYPE html>

<html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <title>Clientes mayores de edad</title>
</head>

<body class="container-fluid">

    <div class="row">
        <h1 class= "col-12">Clientes mayores de edad</h1>

        <p class="col-12">{{ date('m/d/Y') }}</p>
    </div>

    
 <div class="row">
    <table class="table table-striped">
        <thead>
            <th scope="col">Nombre</th>
            <th scope="col">Correo</th>
            <th scope="col">Fecha de nacimiento</th>
        </thead>
        <tbody>
            @foreach ($data as $cliente)
                <tr>
                    <td>{{$cliente->nombre}}</td>
                    <td>{{$cliente->correo}}</td>
                    <td>{{$cliente->fecha_nacimiento}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
 </div>
    

</body>

</html>