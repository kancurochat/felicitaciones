<?php

namespace App\Imports;

use App\Models\Cliente;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ClientesImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Cliente([
            'nombre' => $row['nombre'],
            'imagen' => $row['imagen'],
            'correo' => $row['correo'],
            'fecha_nacimiento' => $row['fecha_nacimiento']
        ]);
    }
}
