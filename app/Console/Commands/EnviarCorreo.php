<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class EnviarCorreo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enviar:correo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a email to a customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $arrayClientes = DB::table('clientes')
        ->whereMonth('fecha_nacimiento', date('n'))
        ->whereDay('fecha_nacimiento', date('j'))->get();

        foreach ($arrayClientes as $cliente) {
            $datosCliente = array('name' => $cliente->nombre);
            Mail::send('mail', $datosCliente, function($message) use($cliente) {
                $message->to($cliente->correo, $cliente->nombre)->subject(env('APP_NAME'));
                $message->from(env('MAIL_USERNAME'));
                $message->attach(asset($cliente->imagen));
            });
        }
    }
}
