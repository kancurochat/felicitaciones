<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\DB;

class PDFController extends Controller
{
        /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function generatePDF()

    {
          $data = DB::table('clientes')
          ->select()
          ->whereYear('fecha_nacimiento', '<=', (date('Y')-18))
          ->whereMonth('fecha_nacimiento', '<', date('n'))
          ->whereDay('fecha_nacimiento', '<', date('j'))
          ->get();

        $pdf = PDF::loadView('pdf', ['data' => $data]);

    

        return $pdf->download('itsolutionstuff.pdf');

    }
}
