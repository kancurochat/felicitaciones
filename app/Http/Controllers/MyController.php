<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\ClientesExport;
use App\Imports\ClientesImport;
use Maatwebsite\Excel\Facades\Excel;

class MyController extends Controller
{
    /**

     * @return \Illuminate\Support\Collection

     */
    public function importExportView()
    {
        return view('import');
    }

    /**

     * @return \Illuminate\Support\Collection

     */
    public function export()
    {
        return Excel::download(new ClientesExport, 'clientes.xlsx');
    }

    /**

     * @return \Illuminate\Support\Collection

     */
    public function import()
    {
        Excel::import(new ClientesImport, request()->file('file'));
        return back();
    }
}
