<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class MailController extends Controller
{

    public function felicita()
    {
        $arrayClientes = DB::table('clientes')
        ->whereMonth('fecha_nacimiento', date('n'))
        ->whereDay('fecha_nacimiento', date('j'))->get();

        foreach ($arrayClientes as $cliente) {
            $datosCliente = array('name' => $cliente->nombre);
            Mail::send('mail', $datosCliente, function($message) use($cliente) {
                $message->to($cliente->correo, $cliente->nombre)->subject(env('APP_NAME'));
                $message->from(env('MAIL_USERNAME'));
                $message->attach(asset($cliente->imagen));
            });
        }

        echo 'Email sent with congratulations! Check your inbox.';

    }

    public function basic_email()
    {
        $data = array('name' => "Virat Gandhi");

        Mail::send(['text' => 'mail'], $data, function ($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject('Laravel Basic Testing Mail');
            $message->from('xyz@gmail.com', 'Virat Gandhi');
        });
        echo "Basic Email Sent. Check your inbox.";
    }
    public function html_email()
    {
        $data = array('name' => "Virat Gandhi");
        Mail::send('mail', $data, function ($message) {
            $message->to('dswprueba@gmail.com', 'Tutorials Point')->subject('Laravel HTML Testing Mail');
            $message->from('dswprueba@gmail.com', 'Virat Gandhi');
        });
        echo "HTML Email Sent. Check your inbox.";
    }
    public function attachment_email()
    {
        $data = array('name' => "Virat Gandhi");
        Mail::send('mail', $data, function ($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject('Laravel Testing Mail with Attachment');
            $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
            $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
            $message->from('xyz@gmail.com', 'Virat Gandhi');
        });
        echo "Email Sent with attachment. Check your inbox.";
    }
}
