<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ClienteFormRequest;
use Illuminate\Support\Facades\DB;

class CatalogController extends Controller
{

    public function getIndex(Request $request) {
        
        $texto = trim($request->get('texto'));
        $arrayClientes = DB::table('clientes')
        ->select()
        ->where('nombre', 'LIKE', '%'.$texto.'%')->paginate(12);
        
        return view('catalog/index', compact('arrayClientes', 'texto'));
    
    }

    public function getCreate() {
        return view('catalog/create');
    }

    public function postCreate(Request $request) {

        $cliente = new Cliente();
        $cliente->nombre = $request->input('nombre');

       /*  $request->validate([
            'imagen' => 'required|mimes:png,jpg|max:2048'
        ]); */

        $cliente->fecha_nacimiento = $request->input('fecha_nacimiento');
        $cliente->correo = $request->input('correo');

        
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'correo' => 'required|email:rfc,dns',
            'fecha_nacimiento' => 'required',
            'imagen' => 'required|mimes:png,jpg|max:2048'
        ]);

        if($validator->fails()){
            return redirect('catalog/create')
                        ->withErrors($validator)
                        ->withInput();
        };

        $request->file('imagen')->store('public');
        
        $cliente->imagen = asset('storage/'.$request->file('imagen')->hashName());


        

        $cliente->save();

        $request->session()->flash('correcto', 'Se ha creado el registro');

        return redirect('catalog');

    }

    public function getShow($id) {
        return view('catalog/show', array('cliente' => Cliente::findOrFail($id)));
    }

    public function getEdit($id) {
        return view('catalog/edit', array('cliente' => Cliente::findOrFail($id)));
    }

    public function putEdit(ClienteFormRequest $request, $id) {
        $cliente =  Cliente::findOrFail($id);
        
        $cliente->nombre = $request->input('nombre');
        
        $request->validate([
            'imagen' => 'required|mimes:png,jpg|max:2048'
        ]);
        $request->file('imagen')->store('public');
        
        $cliente->fecha_nacimiento = $request->input('fecha_nacimiento');
        $cliente->correo = $request->input('correo');

        $cliente->imagen = asset('storage/'.$request->file('imagen')->hashName());

        $cliente->save();

        $request->session()->flash('editOK', 'Se ha editado correctamente el registro');

        return redirect('catalog');
    }

    public function putDelete(Request $request, $id) {
        $cliente = Cliente::findOrFail($id);

        $cliente->delete();

        $request->session()->flash('deleteOK', 'Se ha borrado un cliente con éxito');

        return redirect('catalog');
    }

}
