<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'correo' => 'required|email:rfc,dns',
            'fecha_nacimiento' => 'required',
            'imagen' => 'required|mimes:png,jpg|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Pon un nombre, tolete',
            'correo.required' => 'Pon un correo, tolete',
            'correo.email' => 'Al menos esfuérzate en poner algo que se parezca a un email, tolete',
            'fecha_nacimiento.required' => 'Que me digas cuando naciste, tolete',
            'imagen.required' => 'Que me pases una foto, tolete'
        ];
    }
}
